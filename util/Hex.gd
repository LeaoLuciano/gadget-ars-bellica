class_name Hex extends Object

const DIR2HEX = [
	Vector2(0, 1),
	Vector2(-1, 1),
	Vector2(-1, 0),
	Vector2(0, -1),
	Vector2(1, -1),
	Vector2(1, 0),
]

static func distance(a: Vector2, b: Vector2) -> int:
	return int((abs(a.x - b.x) 
			+ abs(a.x + a.y - b.x - b.y)
			+ abs(a.y - b.y)) / 2)

static func closest_dir(diff: Vector2) -> int:
	var closest_alignment := -INF
	var closest := -1
	for dir in DIR2HEX.size():
		var alignment := diff.dot(DIR2HEX[dir].normalized())
		if alignment > closest_alignment:
			closest_alignment = alignment
			closest = dir
	return closest
