class_name SignalHelper extends Object

static func multi_connect(object: Object, observer: Object, \
						  signal_map: Dictionary):
	for signal_name in signal_map:
		var method_name := signal_map[signal_name] as String
		safe_connect(object, signal_name, observer, method_name)

static func multi_disconnect(object: Object, observer: Object, \
							 signal_map: Dictionary):
	for signal_name in signal_map:
		var method_name := signal_map[signal_name] as String
		safe_disconnect(object, signal_name, observer, method_name)

static func safe_connect(object: Object, signal_name: String, observer: Object, \
						 method_name: String, args := [], flags := 0):
	if not object.is_connected(signal_name, observer, method_name):
		#warning-ignore:return_value_discarded
		object.connect(signal_name, observer, method_name, args, flags)

static func safe_disconnect(object: Object, signal_name: String, \
							observer: Object, method_name: String):
	if object.is_connected(signal_name, observer, method_name):
		object.disconnect(signal_name, observer, method_name)

static func reconnect(object: Object, signal_name: String, observer: Object, \
					  method_name: String, args := [], flags := 0):
	safe_disconnect(object, signal_name, observer, method_name)
	#warning-ignore:return_value_discarded
	object.connect(signal_name, observer, method_name, args, flags)
