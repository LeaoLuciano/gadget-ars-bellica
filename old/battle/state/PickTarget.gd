class_name PickTarget extends State

var command: Command
var ability_use: AbilityUse
var success := false
var alternate_spell: Spell = null

func with_command(cmd: Command, use: AbilityUse) -> PickTarget:
	command = cmd
	ability_use = use
	return self

func _enter(_previous: State):
	success = false
	alternate_spell = null

func _leave():
	battle.get_map().highlight([])

func _on_cancel(_request: ControlRequest):
	success = false
	pop()

func _on_spell_selected(request: SpellSelectedRequest):
	success = false
	alternate_spell = request.spell
	pop()
