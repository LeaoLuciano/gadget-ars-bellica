extends Reference

class_name Tile

const MAX_DIST = 99
const SIZE = 16
const INVALID := -Vector2.ONE * 99

enum DIR {
	UP, DOWN, LEFT, RIGHT, UPLEFT, DOWNRIGHT, UPRIGHT, DOWNLEFT, ALL
}

const DIR_HINT := "UP,DOWN,LEFT,RIGHT,UPLEFT,DOWNRIGHT,UPRIGHT,DOWNLEFT"

const FROM_DIR = [
	Vector2(0, -1),
	Vector2(0, 1),
	Vector2(-1, 0),
	Vector2(1, 0),
	Vector2(-1, -1),
	Vector2(1, 1),
	Vector2(1, -1),
	Vector2(-1, 1),
]

static func center_offset() -> Vector2:
	return SIZE / 2.0 * Vector2.ONE

static func distance(tile1: Vector2, tile2: Vector2) -> int:
	var diff := tile1 - tile2
	return int(max(abs(diff.x), abs(diff.y)))

static func length(tile: Vector2) -> int:
	return distance(Vector2.ZERO, tile)

static func to_closest_dir(tile_diff: Vector2) -> int:
	var largest = -1
	var closest_dir = -1
	for dir in range(DIR.ALL):
		var diff = FROM_DIR[dir]
		var dot = tile_diff.dot(diff.normalized())
		if dot > largest:
			closest_dir = dir
			largest = dot
	return closest_dir

static func trace_line(from: Vector2, to: Vector2) -> Array:
	var line := []
	var diff := to - from
	if from == to:
		return [from, to]
	var n
	var step
	if abs(diff.y) <= abs(diff.x):
		n = abs(diff.x) + 1
		step = Vector2(sign(diff.x), diff.y / abs(diff.x))
	else:
		n = abs(diff.y) + 1
		step = Vector2(diff.x / abs(diff.y), sign(diff.y))
	for i in range(n):
		var x = round(i * step.x)
		var y = round(i * step.y)
		var tile = from + Vector2(x, y)
		line.append(tile)
	return line
