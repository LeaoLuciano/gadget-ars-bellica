class_name CursorController extends Reference

signal cursor_tile_set(position, is_inside)

var map: TileMap = null
var map_info: MapInfo = null
var map_cursor: Node2D = null

var map_position := Vector2.ZERO

func _init(_map: TileMap, _info: MapInfo, _cursor: Node2D):
	self.map = _map
	self.map_info = _info
	self.map_cursor = _cursor

func update_cursor(cursor_position: Vector2, delta: float) -> void:
	var raw_position := cursor_position
	self.map_position.x = clamp(cursor_position.x, 0, self.map_info.size - 1)
	self.map_position.y = clamp(cursor_position.y, 0, self.map_info.size - 1)

	var target_screen_position = self.map.map_to_world(self.map_position)
	var is_inside_map: bool = self.map_info.is_tile_inside_map(raw_position)
	var dislocation: Vector2 = target_screen_position - self.map_cursor.position

	self.map_cursor.visible = is_inside_map
	self.map_cursor.position += dislocation * self.map_info.smooth * delta
	emit_signal("cursor_tile_set", self.map_position, is_inside_map)
