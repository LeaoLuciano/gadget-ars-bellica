extends TextureRect

export(Array, Texture) var TEXTURES

onready var value := 2

func _process(_delta):
	self.texture = TEXTURES[self.value]
