tool
class_name Synth extends DSPNode

enum WaveForm {
	SINE, TRIANGLE, SQUARE
}

export(WaveForm)		var wave_form
export(float, 0.0, 1.0)	var wave_dist = 0.5
export 					var bpm := 200
export(float, 0.0, 1.0) var volume := 1.0

## Envelope vars
var attack := 200
var decay := 200
var sustain := 0.5
var release := 200

var playing := false
var freq := 0.0
var env := 0.0
var phase := 0.0

func _get_property_list():
	var properties := []
	for field in ["attack", "decay", "sustain", "release"]:
		properties.append({
			name = "envelope/" + field,
			type = TYPE_REAL,
			usage = PROPERTY_USAGE_DEFAULT,
			hint = PROPERTY_HINT_RANGE,
			hint_string = "0.0,1.0" if field == "sustain" else "0.0,2000.0"
		})
	return properties

func _get(field: String):
	if field.begins_with("envelope/"):
		return get(field.trim_prefix("envelope/"))

func _set(field: String, value):
	if field.begins_with("envelope/"):
		return set(field.trim_prefix("envelope/"), value)

func fill_block(block: DSPBlock):
	var delta := block.get_sample_size() * 1000
	for i in DSPBlock.BLOCK_SIZE:
		var amp := 0.0
		if playing:
			if env < attack:
				amp = env / attack
			elif env >= attack and env < attack + decay:
				amp = 1.0 - (1.0 - sustain) * (env - attack) / decay
			elif env >= attack + decay:
				amp = sustain
		else:
			if env < release:
				amp = sustain * (1.0 - env / release)
		env += delta
		var increment = freq / block.frame_rate
		var frame = amp * volume * Vector2.ONE * pulse(phase)
		block.samples[i] += frame
#		block.samples[i] += amp * Vector2.ONE
		phase = fmod(phase + increment, 1.0)
#
func pulse(t) -> float:
	if t < wave_dist:
		t = 0.5 * t / wave_dist
	else:
		t = 0.5 * (1.0 + (t - wave_dist) / (1 - wave_dist))
	match wave_form:
		WaveForm.SINE:
			return sin(t * TAU)
		WaveForm.TRIANGLE:
			if t < 0.5:
				return -1.0 + t * 4.0
			else:
				return (1.0 - t) * 4 - 1.0
		WaveForm.SQUARE:
			return -1.0 if t < 0.5 else 1.0
	return 0.0

func play(midi := 69):
	freq = pow(2.0, (midi - 69.0) / 12.0) * 440.0
	playing = true
	env = 0.0

func stop():
	playing = false
	env = 0.0

#static func fill_buffer(node: Node, playback: AudioStreamPlayback):
#	var to_fill = min(SAMPLE_MAX_SIZE, playback.get_frames_available())
#	var derp := []
#	derp.resize(to_fill)
#
#	var buffer := SampleBuffer.new()
#	buffer.rate = 44100.0
#	buffer.frames = PoolVector2Array(derp)
#
#	for i in to_fill:
#		buffer.frames.append(Vector2.ZERO)
#
#	for child in node.get_children():
#		if child.has_method("_fill_buffer"):
#			child._fill_buffer(buffer)
#
#	for i in to_fill:
#		playback.push_frame(buffer.frames[i])
#
#	buffer.free()
