class_name DSPBlock extends Object

const BLOCK_SIZE := 1024
const DERP := []

var frame_rate: float
var samples: PoolVector2Array

func _init(rate := 44100.0):
	if DERP.size() != BLOCK_SIZE:
		DERP.resize(BLOCK_SIZE)
		for i in BLOCK_SIZE:
			DERP[i] = 0.0
	samples = PoolVector2Array(DERP)
	frame_rate = rate

func get_sample_size() -> float:
	return 1.0 / frame_rate

func map(f: FuncRef):
	for i in BLOCK_SIZE:
		samples[i] = f.call_func(samples[i])

func zip(block: DSPBlock, f: FuncRef):
	for i in BLOCK_SIZE:
		samples[i] = f.call_func(samples[i], block.samples[i])

func flush(playback: AudioStreamPlayback):
	for sample in samples:
		playback.push_frame(sample)

static func ADD(v: Vector2, w: Vector2) -> Vector2:
	return v + w

static func MUL(v: Vector2, w: Vector2) -> Vector2:
	return v * w
