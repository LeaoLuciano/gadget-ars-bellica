class_name Persist extends Node

export var BATTLE_SCN: PackedScene

const SAVE_PATH := "user://save.tres"

var current_state: CampaignState

onready var scheduled := false
onready var mutex := Mutex.new()
onready var threads := []

func _exit_tree():
	for thread in threads:
		thread.wait_to_finish()

func has_save():
	var file := File.new()
	var ok := false
	if file.open(SAVE_PATH, File.READ) == OK:
		ok = true
		file.close()
	return ok

func schedule_save(campaign: Campaign_old):
	if scheduled:
		return
	scheduled = true
	while scheduled and mutex.try_lock() != OK:
		yield(get_tree(), "idle_frame")
	if current_state == null:
		current_state = CampaignState.new()
	current_state.save(campaign)
	var thread := Thread.new()
	threads.append(thread)
	assert(thread.start(self, "_save_in_background") == OK)
	scheduled = false
	while thread.is_active():
		yield(get_tree(), "idle_frame")
	thread.wait_to_finish()

func load(campaign: Campaign_old):
	mutex.lock()
	var state := ResourceLoader.load(SAVE_PATH) as CampaignState
	state.load(campaign, BATTLE_SCN)
	current_state = state
	mutex.unlock()

func _save_in_background(_unused):
	var ok = ResourceSaver.save(SAVE_PATH, current_state)
	mutex.unlock()
	assert(ok == OK, "error %d" % ok)
