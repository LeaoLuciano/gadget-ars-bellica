class_name EntityState extends Resource

export(String, FILE)		var scene_path
export(String)				var node_name
export(String)				var entity_name
export(Vector2)				var tile
export(Resource)			var movement_profile
export(Array, PackedScene)	var extra_pre_properties
export(Array, PackedScene)	var extra_post_properties
export(Dictionary)			var storage

func save(entity: Entity):
	scene_path = entity.filename
	node_name = entity.name
	entity_name = entity.entity_name
	tile = entity.tile
	movement_profile = entity.movement_profile
	var pre_ended := false
	for property in entity.get_children():
		if property is Property:
			if property.owner != entity:
				if not pre_ended:
					extra_pre_properties.push_front(property.filename)
				else:
					extra_post_properties.append(property.filename)
			else:
				pre_ended = true
			var prop_storage := {}
			property._save(prop_storage)
			storage[entity.get_path_to(property)] = prop_storage

func load_entity() -> Entity:
	var scene := load(scene_path) as PackedScene
	var entity := scene.instance() as Entity
	entity.name = node_name
	entity.entity_name = entity_name
	entity.tile = tile
	entity.movement_profile = movement_profile
	for property_scn_path in extra_pre_properties:
		var property_scn := load(property_scn_path) as PackedScene
		var property := property_scn.instance() as Property
		entity.add_property(property, true)
	for property_scn_path in extra_post_properties:
		var property_scn := load(property_scn_path) as PackedScene
		var property := property_scn.instance() as Property
		entity.add_property(property)
	for entry in storage:
		var path := entry as NodePath
		var prop_storage := storage[path] as Dictionary
		var property := entity.get_node(path)
		for field in prop_storage:
			property.set(field, prop_storage[field])
	return entity
