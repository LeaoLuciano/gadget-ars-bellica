class_name WorldMap extends GridContainer

class ChunkCache extends Reference:
	var scene: PackedScene
	var node: Battle
	func _init(the_scene: PackedScene):
		scene = the_scene
	func get_instance() -> Battle:
		if node == null and scene != null:
			node = scene.instance() as Battle
			node.get_map().load_environment()
		return node

func compile() -> Dictionary:
	var world_map := {}
	for k in get_child_count():
		var y := int(k) / columns
		var x := int(k) % columns
		var chunk_scene := (get_child(k) as Chunk).scene
		var chunk_cache := ChunkCache.new(chunk_scene)
		world_map[Vector2(x, y)] = chunk_cache
	return world_map
