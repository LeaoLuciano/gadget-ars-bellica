extends Node2D

export var SPEED := 100

var source_path: NodePath

signal hit

func _ready():
	var source := get_node(source_path) as Node2D
	$Sprite.position = (source.position + Tile.center_offset()) - position
	$Sprite.show()
	$Sprite/CPUParticles2D.emitting = true

func set_source_path(path: NodePath):
	source_path = path

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var sprite := $Sprite as Sprite
	var dir := -sprite.position.normalized() as Vector2
	var motion = dir * SPEED * delta
	if motion.length_squared() > sprite.position.length_squared():
		sprite.position = Vector2.ZERO
		emit_signal("hit")
		queue_free()
	else:
		sprite.position += motion
