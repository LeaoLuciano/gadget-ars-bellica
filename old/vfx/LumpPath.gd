extends AnimatedSprite

export var lump_scn: PackedScene
export var lump_delay := 0.2

signal hit

var source_path: NodePath

func set_source_path(path: NodePath):
	source_path = path

func _start_vfx(map: Map):
	var source_entity := get_node(source_path) as Entity
	var target_tile := map.world_to_map(position)
	var line := Physics \
		.new(map) \
		.hitscan(source_entity, source_entity.tile, target_tile, false)
	for tile in line:
		var lump := lump_scn.instance()
		map.add_vfx_at(lump, tile)
		yield(get_tree().create_timer(lump_delay), "timeout")
	emit_signal("hit")
	queue_free()
