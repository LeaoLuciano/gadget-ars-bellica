extends Property

export(String, MULTILINE) var flavor_text := "Something..."

func _process(_delta):
	INFO = 'it reads:\n\n"%s"' % flavor_text

func _save(storage: Dictionary):
	storage["flavor_text"] = flavor_text
