extends Rule

func _apply_effect(_map: Map, effect: Effect):
	var stat_effect := effect as ChangeStatEffect
	if stat_effect.get_stat_type() == Stats.Type.HIT_POINTS:
		var destructible := get_self_property() as Destructible
		destructible.change_by(stat_effect.get_variation())
		if destructible.is_dead():
			effect = effect.then(DestroyEffect.new().on(get_self_entity()))
