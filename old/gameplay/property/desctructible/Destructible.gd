class_name Destructible extends Property

export(int, 0, 9999) var max_hp := 3
export(int) var damage := 0

func get_hp() -> int:
	return self.max_hp - self.damage

func is_dead() -> bool:
	return self.damage >= self.max_hp

func change_by(amount: int):
	damage = max(min(damage - amount, max_hp), 0) as int

func _process(_delta):
	INFO = "destructible (%d/%d)" % [get_hp(), max_hp]

func _save(storage: Dictionary):
	storage["max_hp"] = max_hp
	storage["damage"] = damage
