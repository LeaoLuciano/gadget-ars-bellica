extends AI

onready var walk_ability := AbilityHelper.find_ability(self)

func _process_property(entity: Entity, map: Map):
	var actor := entity.get_property(Actor) as Actor
	var direction := entity.get_property(Direction) as Direction
	if actor.next_action == null and direction != null:
		var dir := direction.direction as int
		var entity_tile = entity.tile
		var movement = Tile.FROM_DIR[dir] as Vector2
		var walk := AbilityUse.new(walk_ability, map, entity) \
			.with_target_tile(entity_tile + movement)
		actor.next_action = walk
