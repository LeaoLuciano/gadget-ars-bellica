extends AI

onready var walk_ability := AbilityHelper.find_ability(self)

func _process_property(entity: Entity, map: Map):
	var actor := entity.get_property(Actor) as Actor
	if actor.next_action == null:
		var dir = randi() % Tile.DIR.ALL
		var entity_tile = entity.tile
		var movement = Tile.FROM_DIR[dir]
		var walk := AbilityUse.new(walk_ability, map, entity) \
			.with_target_tile(entity_tile + movement)
		actor.next_action = walk
