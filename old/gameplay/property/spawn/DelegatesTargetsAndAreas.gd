extends Rule

func _process_effect(_map: Map, effect: Effect) -> Effect:
	var spawn := get_self_property() as Spawn
	var spawner := get_node_or_null(spawn.spawner_path) as Entity
	if spawner != null:
		var delegated_effect := effect.duplicate().on(spawner)
		return delegated_effect
	else:
		return effect
