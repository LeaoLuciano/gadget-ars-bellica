extends TriggerRule

export(int, 1, 20) var max_range := 1
export(float, 0.0, 1.0) var chance := .5
export(String, FILE, "*.tscn") var self_status_scn_path
export(NodePath) var status_cmd_path
export(NodePath) var target_cmd_path

func _ready():
	get_node(status_cmd_path).scene = load(self_status_scn_path)

func _apply_effect(map: Map, _effect: Effect):
	var target_cmd := get_node(target_cmd_path) as OnTarget
	var valid_targets := []
	for x in range(-max_range, max_range):
		for y in range(-max_range, max_range):
			var tile := get_self_entity().tile + Vector2(x, y)
			for entity in map.find_all_entities_at(tile):
				if entity == get_self_entity(): continue
				var preview := TargetEffect.new() \
					.with_profile(target_cmd.movement_profile) \
					.target(entity) \
					.cause(EnergyEffect.new()) \
					.on(get_self_entity())
				if EffectSolver.preview(map, preview).is_succesfull():
					valid_targets.append(entity)
	if not valid_targets.empty() and randf() <= chance:
		var target := valid_targets[randi() % valid_targets.size()] as Entity
		trigger_ability(map, {
			AbilityTarget.FIELD.TARGET_STATUS: property_path,
			AbilityTarget.FIELD.TARGET_ENTITY: target.get_path()
		})
