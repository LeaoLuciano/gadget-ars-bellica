extends Rule

func _apply_effect(_map: Map, effect: Effect):
	var status := get_self_property() as Status
	if effect is NewRoundEffect:
		if status._tick():
			var remove_effect := RemoveStatusEffect.new() \
					.remove_status(status.get_path()) \
					.on(get_self_entity())
			effect = effect.then(remove_effect)
