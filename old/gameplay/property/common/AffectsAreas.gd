extends Rule

func _process_effect(map: Map, effect: Effect) -> Effect:
	var area_effect := effect as AreaEffect
	var radius := area_effect.get_radius()
	var tile_range = range(-radius, radius+1)
	var profile := area_effect.get_movement_profile()
	var each_effect := area_effect as Effect
	var tiles := []
	for y in tile_range:
		for x in tile_range:
			var diff := Vector2(x, y)
			var tile := area_effect.get_tile() + diff
			tiles.append(tile)
	for tile in tiles:
		for entity in map.find_all_entities_at(tile):
			if Physics.new(map).hits(profile, entity):
				each_effect = each_effect.then(
					area_effect.get_caused_effect().on(entity)
				)
				each_effect = each_effect.next
	return effect
