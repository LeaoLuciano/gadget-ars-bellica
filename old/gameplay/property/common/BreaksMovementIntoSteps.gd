extends Rule

func _process_effect(map: Map, effect: Effect) -> Effect:
	var move_effect := effect as MoveEffect
	var entity := move_effect.get_entity()
	var target_tile := move_effect.get_tile()
	if move_effect.is_relative():
		target_tile = entity.tile + target_tile
	var profile := (entity.movement_profile as MovementProfile).compose(
		move_effect.get_movement_profile()
	)
	move_effect = move_effect.with_profile(profile)
	if target_tile == entity.tile:
		return effect
	var movement := Physics.new(map).hitscan(profile, entity.tile, target_tile)
	if movement.empty():
		return move_effect.prevent()
	move_effect = move_effect.to(movement.back())
	var step = move_effect
	for i in movement.size():
		var tile := movement[i] as Vector2
		var step_effect := ChangePositionEffect.new() \
			.relocate(entity) \
			.to(tile) \
			.on(map.get_environment())
		step = step.then(step_effect).next
	return effect
