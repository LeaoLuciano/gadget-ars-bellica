extends Rule

func _apply_effect(map: Map, effect: Effect):
	var travel_effect := effect as TravelEffect
	var direction := travel_effect.get_direction()
	map.exit_map(Tile.FROM_DIR[direction])
