extends Rule

func _apply_effect(_map: Map, effect: Effect):
	var roll_effect := effect as RollInitiativeEffect
	var turn := get_self_property() as Turn
	turn.initiative = Dice.roll(2, 6) + roll_effect.get_speed()
