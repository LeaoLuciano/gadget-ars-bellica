class_name ArcaneScholar extends Property

const MAX_SPELLS := 5

export(Array, PackedScene) var spell_list

signal grimoire_changed

onready var grimoire := $Grimoire

var real_spell_list := [] # because the exported one has shared state
var loaded_first_time := false

func _ready():
	if not loaded_first_time:
		for spell_scn in spell_list:
			store_spell(spell_scn)
		loaded_first_time = true
	emit_signal("grimoire_changed")

func get_spell_list() -> Array:
	return grimoire.get_children()

func can_store_spell(spell_scn: PackedScene) -> bool:
	return real_spell_list.size() < MAX_SPELLS \
	   and not spell_scn in real_spell_list

func store_spell(spell_scn: PackedScene):
	var spell := spell_scn.instance() as Spell
	grimoire.add_child(spell)
	real_spell_list.append(spell_scn)
	emit_signal("grimoire_changed")

func erase_spell(spell: Spell):
	if spell != null and grimoire.is_a_parent_of(spell):
		var spell_scn := load(spell.filename) as PackedScene
		real_spell_list.erase(spell_scn)
		spell.free()
		emit_signal("grimoire_changed")

func _save(storage: Dictionary):
	storage["spell_list"] = real_spell_list
