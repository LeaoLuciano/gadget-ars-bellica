extends Rule

func _process_effect(_map: Map, effect: Effect) -> Effect:
	var controller := get_self_property() as Controller
	var new_effect := effect
	for path in controller.controlled_paths:
		var spawn := get_node_or_null(path) as Entity
		if spawn != null:
			new_effect = new_effect.then(DestroyEffect.new().on(spawn)).next
	return effect
