class_name ControlHelper extends Object

static func is_controlled_by(controlled_path: NodePath, controller: Entity) -> bool:
	if controller.has_property(Controller):
		var controlled_paths = controller.get_property(Controller).controlled_paths
		return controlled_path in controlled_paths
	else:
		return false
