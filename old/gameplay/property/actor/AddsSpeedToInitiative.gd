extends Rule

func _process_effect(_map: Map, effect: Effect) -> Effect:
	var roll_effect := effect as RollInitiativeEffect
	var actor := get_self_property() as Actor
	roll_effect = roll_effect.change_speed_by(actor.SPEED)
	if actor.next_action != null:
		var mod := actor.next_action.ability.action_speed_modifier
		return roll_effect.change_speed_by(mod)
	else:
		return effect
