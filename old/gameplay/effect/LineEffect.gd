class_name LineEffect extends Effect

const MIRROR := "mirror"

static func _required_fields() -> Array:
	return [
		EffectData.TYPE.TILE,
		EffectData.TYPE.DIRECTION,
		EffectData.TYPE.VALUE,
		EffectData.TYPE.SUB_EFFECT,
		EffectData.TYPE.MOVEMENT_PROFILE,
		MIRROR
	]

func get_class(): return "LineEffect"

func _init():
	data.movement_profile = MovementProfile.new()

func from_tile(tile: Vector2) -> LineEffect:
	data.tile = tile
	return self

func towards(dir: int) -> LineEffect:
	data.direction = dir
	return self

func with_length(length: int) -> LineEffect:
	data.value = length
	return self

func cause(effect: Effect) -> LineEffect:
	data.sub_effect = effect
	return self

func with_profile(profile: MovementProfile) -> LineEffect:
	data.movement_profile = profile
	return self

func is_mirrored() -> bool:
	return data.has_flag(MIRROR)

func get_tile() -> Vector2:
	return data.tile

func get_direction() -> int:
	return data.direction

func get_length() -> int:
	return data.value

func get_caused_effect() -> Effect:
	return data.sub_effect.duplicate() as Effect

func get_movement_profile() -> MovementProfile:
	return data.movement_profile
