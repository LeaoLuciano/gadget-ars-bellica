class_name GiveStatusEffect extends Effect

static func _required_fields() -> Array:
	return [
		EffectData.TYPE.SCENE,
		EffectData.TYPE.VALUE
	]

func get_class(): return "GiveStatusEffect"

func give_status(property_scn: PackedScene) -> GiveStatusEffect:
	data.scene = property_scn
	return self

func for_duration(turns: int) -> GiveStatusEffect:
	data.value = turns
	return self

func from_source(path: NodePath) -> GiveStatusEffect:
	data.node_path = path
	return self

func get_status() -> PackedScene:
	return data.scene

func get_duration() -> int:
	return data.value

func get_source_path() -> NodePath:
	return data.node_path
