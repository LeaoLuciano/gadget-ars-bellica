class_name RollInitiativeEffect extends Effect

static func _required_fields() -> Array:
	return [
		EffectData.TYPE.VALUE,
	]

func get_class(): return "RollInitiativeEffect"

func with_speed(speed: int = 1) -> RollInitiativeEffect:
	data.value = speed
	return self

func change_speed_by(by: int) -> RollInitiativeEffect:
	data.value += by
	return self

func get_speed() -> int:
	return data.value
