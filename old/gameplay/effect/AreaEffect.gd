class_name AreaEffect extends Effect

static func _required_fields() -> Array:
	return [
		EffectData.TYPE.TILE,
		EffectData.TYPE.VALUE,
		EffectData.TYPE.SUB_EFFECT,
		EffectData.TYPE.MOVEMENT_PROFILE
	]

func get_class(): return "AreaEffect"

func _init():
	data.movement_profile = MovementProfile.new()

func on_tile(tile: Vector2) -> AreaEffect:
	data.tile = tile
	return self

func with_radius(radius: int) -> AreaEffect:
	data.value = radius
	return self

func cause(effect: Effect) -> AreaEffect:
	data.sub_effect = effect
	return self

func with_profile(profile: MovementProfile) -> AreaEffect:
	data.movement_profile = profile
	return self

func get_tile() -> Vector2:
	return data.tile

func get_radius() -> int:
	return data.value

func get_caused_effect() -> Effect:
	return data.sub_effect.duplicate() as Effect

func get_movement_profile() -> MovementProfile:
	return data.movement_profile
