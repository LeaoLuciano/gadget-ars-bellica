class_name TargetEffect extends Effect

static func _required_fields() -> Array:
	return [
		EffectData.TYPE.NODE_PATH,
		EffectData.TYPE.SUB_EFFECT,
		EffectData.TYPE.MOVEMENT_PROFILE
	]

func get_class(): return "TargetEffect"

func _init():
	data.movement_profile = MovementProfile.new()

func target(entity: Entity) -> TargetEffect:
	data.node_path = entity.get_path()
	return self

func cause(effect: Effect) -> TargetEffect:
	data.sub_effect = effect
	return self

func with_profile(profile: MovementProfile) -> TargetEffect:
	data.movement_profile = profile
	return self

func get_target_entity_path() -> NodePath:
	return data.node_path

func get_caused_effect() -> Effect:
	return data.sub_effect.duplicate() as Effect

func get_movement_profile() -> MovementProfile:
	return data.movement_profile
