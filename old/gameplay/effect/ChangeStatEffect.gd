class_name ChangeStatEffect extends Effect

static func _required_fields() -> Array:
	return [
		EffectData.TYPE.STAT, EffectData.TYPE.VALUE,
	]

func get_class(): return "ChangeStatEffect"

func stat(which: int) -> ChangeStatEffect:
	data.stat = which
	return self

func by(amount: int) -> ChangeStatEffect:
	data.value = amount
	return self

func get_stat_type() -> int:
	return data.stat

func get_variation() -> int:
	return data.value
