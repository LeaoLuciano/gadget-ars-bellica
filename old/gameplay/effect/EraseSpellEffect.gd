class_name EraseSpellEffect extends Effect

static func _required_fields() -> Array:
	return [
		EffectData.TYPE.NODE_PATH,
	]

func get_class(): return "EraseSpellEffect"

func erase_spell(path: NodePath) -> EraseSpellEffect:
	data.node_path = path
	return self

func get_spell_path() -> NodePath:
	return data.node_path
