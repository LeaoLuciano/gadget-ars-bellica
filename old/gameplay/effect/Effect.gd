class_name Effect extends Reference

const DIR_PATH := "res://old/gameplay/effect"

enum {
	PREVENTED,
	SUCCESFULL,
}

var affected_entity: Entity = null
var data := EffectData.new()
var status := SUCCESFULL
var next: Effect = null

func get_class(): return "Effect"

func on(entity: Entity) -> Effect:
	affected_entity = entity
	return self

func get_entity() -> Entity:
	return affected_entity

func prevent() -> Effect:
	status = PREVENTED
	return self

func is_succesfull() -> bool:
	return status == SUCCESFULL

func is_prevented() -> bool:
	return status == PREVENTED

func then(effect: Effect) -> Effect:
	var it := effect
	while it.next != null:
		it = it.next
	it.next = next
	next = effect
	return self

func then_after(effect: Effect) -> Effect:
	var it := self
	while it.next != null:
		it = it.next
	it.next = effect
	return self

func duplicate() -> Effect:
	var new := get_script().new() as Effect
	new.set_script(get_script())
	new.data = data.duplicate()
	return new

func dump() -> String:
	var path
	if affected_entity != null:
		path = affected_entity.get_path()
	else:
		path = "???"
	var info := "%s -> %s " % [get_class(), path]
	if status == PREVENTED:
		info += "(prevented) "
	var fields := {}
	data.copy_fields_to(fields)
	return info + JSON.print(fields, "\t")

func _to_string() -> String:
	return dump()

