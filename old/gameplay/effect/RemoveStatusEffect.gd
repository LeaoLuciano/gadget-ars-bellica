class_name RemoveStatusEffect extends Effect

static func _required_fields() -> Array:
	return [
		EffectData.TYPE.NODE_PATH,
	]

func get_class(): return "RemoveStatusEffect"

func remove_status(path: NodePath) -> RemoveStatusEffect:
	data.node_path = path
	return self

func get_status_path() -> NodePath:
	return data.node_path
