extends TriggerRule

func _apply_effect(map: Map, _effect: Effect):
	var self_entity := get_self_entity()
	var entities := map.find_all_entities_at(self_entity.tile)
	for entity in entities:
		if entity != self_entity:
			trigger_ability(map, {
				AbilityTarget.FIELD.TRIGGERING_ENTITY: entity.get_path()
			})
	
