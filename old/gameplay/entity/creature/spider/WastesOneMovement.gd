extends Rule

onready var enabled := false

signal message(text)

func _process_effect(_map: Map, effect: Effect) -> Effect:
	var self_entity := get_self_entity()
	if enabled and effect is MoveEffect:
		if effect.get_tile() != self_entity.tile:
			var self_property := get_self_property() as Status
			var hit_effect := EnergyEffect.new() \
				.with_energy(1) \
				.with_element(Element.TYPE.IMPACT) \
				.on(self_property.get_source())
			effect = effect.to(self_entity.tile)
			return effect.then(hit_effect)
	elif not enabled and effect is MoveEffect:
		emit_signal(
			"message",
			"The %s is fast and moves despite the web." % self_entity.entity_name
		)
	return effect

func _apply_effect(_map: Map, effect: Effect):
	if effect is NewRoundEffect:
		enabled = true
