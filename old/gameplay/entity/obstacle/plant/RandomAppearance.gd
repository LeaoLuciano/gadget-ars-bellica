extends Property

export(Array, Texture) var sprites: Array = []

var variation := -1

func _entity_ready(entity: Entity):
	if variation == -1:
		variation = randi() % sprites.size()
	entity.texture = sprites[variation]

func _save(storage: Dictionary):
	storage["variation"] = variation
