class_name AbilityUse extends Reference

const CMD_KEY := "trust me, this is really a command!"

var target := AbilityTarget.new()
var map: Map = null
var actor_entity : Entity = null
var ability: Ability = null

func _init(the_ability: Ability, the_map: Map, the_actor_entity: Entity):
	ability = the_ability
	map = the_map
	actor_entity = the_actor_entity

func with(target_data: Dictionary) -> AbilityUse:
	target.set_from_dict(target_data)
	return self

func with_target_tile(tile: Vector2) -> AbilityUse:
	target.target_offset = tile - actor_entity.tile
	return self

func with_target_direction(dir: int) -> AbilityUse:
	target.target_dir = dir
	return self

func with_triggering_entity(path: NodePath) -> AbilityUse:
	target.triggering_entity_path = path
	return self

func with_target_spell(path: NodePath) -> AbilityUse:
	target.target_spell_path = path
	return self

func pending_command(cmd: Node = null):
	if cmd == null:
		cmd = ability
	for child in cmd.get_children():
		var pending = pending_command(child)
		if pending != null:
			return pending
	if cmd.has_method("get_input_pending") and \
	   cmd.get_input_pending(self) != AbilityTarget.FIELD.NONE:
		return cmd
	else:
		return null

func perform():
	ability.perform(self)
	for cmd in ability.get_children():
		if cmd.KEY_CHECK == CMD_KEY:
			cmd.prepare()
			var funcstate = cmd.perform(self)
			while funcstate is GDScriptFunctionState:
				funcstate = yield(funcstate, "completed")
			if not actor_entity.is_inside_tree():
				return
