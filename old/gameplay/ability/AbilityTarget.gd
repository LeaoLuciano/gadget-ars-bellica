class_name AbilityTarget extends Reference

enum FIELD {
	NONE,
	TARGET_OFFSET,
	TARGET_DIR,
	TRIGGERING_ENTITY,
	TARGET_SPELL,
	TARGET_ENTITY,
	TARGET_STATUS
}

var target_offset := Tile.INVALID
var target_dir: int = Tile.DIR.ALL
var triggering_entity_path := NodePath()
var target_spell_path := NodePath()
var target_entity_path := NodePath()
var target_status_path := NodePath()

func get_field(field: int):
	match field:
		FIELD.TARGET_OFFSET:
			return target_offset
		FIELD.TARGET_DIR:
			return target_dir
		FIELD.TRIGGERING_ENTITY:
			return triggering_entity_path
		FIELD.TARGET_SPELL:
			return target_spell_path
		FIELD.TARGET_ENTITY:
			return target_entity_path
		FIELD.TARGET_STATUS:
			return target_status_path
	return null

func set_from_dict(dict: Dictionary):
	for field in dict:
		match field:
			FIELD.TARGET_OFFSET:
				target_offset = dict[field]
			FIELD.TARGET_DIR:
				target_dir = dict[field]
			FIELD.TRIGGERING_ENTITY:
				triggering_entity_path = dict[field]
			FIELD.TARGET_SPELL:
				target_spell_path = dict[field]
			FIELD.TARGET_ENTITY:
				target_entity_path = dict[field]
			FIELD.TARGET_STATUS:
				target_status_path = dict[field]
