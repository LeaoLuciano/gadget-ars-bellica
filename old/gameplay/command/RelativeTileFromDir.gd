class_name RelativeTileFromDir extends Command

export var MULTIPLIER := 1

func evaluate(use: AbilityUse):
	if get_child_count() < 1: return
	var dir := get_child(0).evaluate(use) as int
	return Tile.FROM_DIR[dir] * MULTIPLIER
