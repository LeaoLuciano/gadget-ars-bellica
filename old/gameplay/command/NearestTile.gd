extends Command

class_name NearestTile

export var WITH_ENTITY := true

func evaluate(use: AbilityUse):
	if get_child_count() < 1: return
	var target_path := get_child(0).evaluate(use) as NodePath
	if !has_node(target_path): return
	var target := get_node(target_path) as Entity
	var nearest_tile = null
	var min_dist := 99
	for entity in use.map.get_all_entities():
		if entity != target:
			var dist = Tile.distance(entity.tile, target.tile)
			if dist < min_dist:
				min_dist = dist
				nearest_tile = entity.tile
	return nearest_tile
