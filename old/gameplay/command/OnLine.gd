tool
class_name OnLine extends Command

enum OriginField {
	SELF_TILE,
}

export(OriginField) var ORIGIN = OriginField.SELF_TILE
export var length := 1
export(Resource) var trajectory_profile
export(Resource) var hit_profile
export var hit_msg := "The %s shoots a beam!"
export var VFX_SCN: PackedScene

signal message(text)

func _ready():
	if trajectory_profile == null:
		trajectory_profile = MovementProfile.new()
	if hit_profile == null:
		hit_profile = MovementProfile.new()

func perform(use: AbilityUse):
	var source_entity := use.actor_entity
	var origin_tile := find_origin(use)
	var dir := use.target.target_dir
	if VFX_SCN != null:
		var end_tile := origin_tile + (Tile.FROM_DIR[dir] * length) as Vector2
		var tiles := Physics \
			.new(use.map) \
			.hitscan(trajectory_profile, origin_tile, end_tile)
		for tile in tiles:
			var vfx := VFX_SCN.instance()
			use.map.add_vfx_at(vfx, tile)
	for cmd in get_children():
		var sub_effect := (cmd as Command).evaluate(use) as Effect
		var hit_effect := TargetEffect.new() \
			.cause(sub_effect) \
			.with_profile(hit_profile)
		var line_effect := LineEffect.new() \
			.cause(hit_effect) \
			.from_tile(origin_tile) \
			.towards(dir) \
			.with_length(length) \
			.with_profile(trajectory_profile) \
			.on(source_entity)
		line_effect = EffectSolver.resolve(use.map, line_effect)
		if line_effect.is_succesfull():
			success()
	if successful:
		emit_signal("message", hit_msg % source_entity.entity_name)
	else:
		emit_signal("message", "Nothing seems to happen...")

func find_origin(use: AbilityUse) -> Vector2:
	var entity := use.actor_entity
	match ORIGIN:
		OriginField.SELF_TILE:
			return entity.tile
	return Tile.INVALID
