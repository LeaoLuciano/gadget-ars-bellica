class_name TileOfEntity extends Command

func evaluate(use: AbilityUse):
	if get_child_count() < 1: return
	var entity_path := get_child(0).evaluate(use) as NodePath
	if !has_node(entity_path):
		return
	var entity := get_node(entity_path) as Entity
	return entity.tile
