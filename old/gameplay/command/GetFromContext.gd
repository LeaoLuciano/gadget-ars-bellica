class_name GetFromContext extends Command

export(AbilityTarget.FIELD) var field

func evaluate(use: AbilityUse):
	return use.target.get_field(field)
