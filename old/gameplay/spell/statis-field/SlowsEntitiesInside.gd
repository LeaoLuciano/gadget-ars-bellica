tool
class_name GivesStatusOverArea extends Rule

export var status_scn: PackedScene

func _ready():
	EFFECT_TYPES = [SenseEffect, DestroyEffect]
	PROCESSES_EFFECT = true

func _process_effect(map: Map, effect: Effect) -> Effect:
	if effect is SenseEffect:
		var subject := get_node_or_null(effect.get_subject_path()) as Entity
		if AreaSensor.entered_area(effect):
			var give_status := GiveStatusEffect.new() \
				.give_status(status_scn) \
				.for_duration(-1) \
				.from_source(entity_path) \
				.on(subject)
			effect = effect.then(give_status)
		elif AreaSensor.exited_area(effect):
			effect = remove_status_effect(effect, subject)
	elif effect is DestroyEffect:
		for entity in map.get_all_entities():
			effect = remove_status_effect(effect, entity)
	return effect

func remove_status_effect(effect: Effect, entity: Entity) -> Effect:
	for property in entity.get_children():
		if		property.filename == status_scn.resource_path \
			and (property as Status).SOURCE_ENTITY_PATH == entity_path:
				var remove_status := RemoveStatusEffect.new() \
					.remove_status(property.get_path()) \
					.on(entity)
				effect = effect.then(remove_status)
	return effect
