extends Rule

export var BONUS := 1

func _process_effect(_map: Map, effect: Effect) -> Effect:
	var sub_effect := effect.data.sub_effect
	if sub_effect is EnergyEffect:
		if sub_effect.has_element(Element.TYPE.IMPACT):
			var old_energy := sub_effect.get_energy() as int
			sub_effect = sub_effect.with_energy(old_energy + BONUS)
	return effect
