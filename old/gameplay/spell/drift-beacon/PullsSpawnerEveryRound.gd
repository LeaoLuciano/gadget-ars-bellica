extends TriggerRule

func _apply_effect(map: Map, _effect: Effect):
	var entity := get_self_entity()
	var spawn := entity.get_property(Spawn) as Spawn
	trigger_ability(map, {
		AbilityTarget.FIELD.TRIGGERING_ENTITY: spawn.spawner_path
	})
