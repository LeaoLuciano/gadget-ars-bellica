tool
extends Rule

export var movement_profile: Resource

func _ready():
	if movement_profile == null:
		movement_profile = MovementProfile.new()

func _process_effect(_map: Map, effect: Effect) -> Effect:
	var move_effect := effect as MoveEffect
	return move_effect.with_profile(movement_profile.duplicate())
