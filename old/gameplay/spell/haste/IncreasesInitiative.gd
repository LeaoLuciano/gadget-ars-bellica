class_name ChangesInitiative extends Rule

export var amount := 2

func _process_effect(_map: Map, effect: Effect) -> Effect:
	var roll_effect := effect as RollInitiativeEffect
	return roll_effect.change_speed_by(amount)
