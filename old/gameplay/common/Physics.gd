class_name Physics extends Reference

var map: Map = null

func _init(the_map: Map):
	map = the_map

func can_enter_tile(entity_or_profile, tile: Vector2) -> bool:
	if map.is_blocked_tile(tile):
		return false
	for other in map.find_all_entities_at(tile):
		if other is Entity:
			if other != entity_or_profile and blocks(entity_or_profile, other):
				return false
	return true

func hitscan(entity_or_profile, from: Vector2, to: Vector2, \
			 include_hit := false) -> Array:
	var line := Tile.trace_line(from, to)
	var result := []
	for i in range(1, line.size()):
		var tile = line[i]
		if not can_enter_tile(entity_or_profile, tile):
			if include_hit:
				result.append(tile)
			break
		else:
			result.append(tile)
	return result

func blocks(entity_or_profile, other: Entity) -> bool:
	var profile1 := as_profile(entity_or_profile)
	var profile2 := eval_movement_profile(other)
	return profile1.matches(profile2)

func hits(entity_or_profile, other: Entity) -> bool:
	var profile1 := as_profile(entity_or_profile)
	var profile2 := eval_movement_profile(other)
	return profile1.is_empty() or profile1.matches(profile2)

func as_profile(entity_or_profile) -> MovementProfile:
	if entity_or_profile is Entity:
		return eval_movement_profile(entity_or_profile)
	elif entity_or_profile is MovementProfile:
		return entity_or_profile
	else:
		return null

func eval_movement_profile(entity: Entity) -> MovementProfile:
	var move_effect := MoveEffect.new() \
		.to(entity.tile) \
		.on(entity)
	move_effect = EffectSolver.preview(map, move_effect) as MoveEffect
	return move_effect.get_movement_profile()
