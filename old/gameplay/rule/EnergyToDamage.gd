tool
class_name EnergyToDamage extends Rule

export(Element.TYPE)	var element_type = Element.TYPE.IMPACT
export(Stats.Type)		var affected_stat = Stats.Type.HIT_POINTS
export(int)				var multiplier = 1
export(PackedScene)		var vfx_scn

func _ready():
	EFFECT_TYPES = [ EnergyEffect ]
	APPLIES_EFFECT = true

func _apply_effect(map: Map, effect: Effect):
	var energy_effect := effect as EnergyEffect
	if 		energy_effect.has_element(element_type) \
		and	energy_effect.get_energy() > 0:
		effect = effect.then(ChangeStatEffect.new() \
			.stat(affected_stat) \
			.by(-multiplier * energy_effect.get_energy()) \
			.on(get_self_entity()))
		if vfx_scn != null:
			map.add_vfx_at(vfx_scn.instance(), get_self_entity().tile)
