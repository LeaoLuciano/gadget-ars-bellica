tool
class_name EnergyImmunity extends Rule

export(Array, Element.TYPE) var elements

signal message(text)

func _ready():
	PROCESSES_EFFECT = true
	EFFECT_TYPES = [EnergyEffect]

func _process_effect(_map: Map, effect: Effect) -> Effect:
	var energy_effect := effect as EnergyEffect
	for element in elements:
		if energy_effect.has_element(element):
			emit_signal("message", "The shield blocked the effect!")
			return effect.prevent()
	return effect
