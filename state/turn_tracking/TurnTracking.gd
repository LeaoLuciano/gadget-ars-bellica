class_name TurnTracking extends State

func _enter(previous: State):
	print(game.simulation.get_turn_order())
	assert(previous.is_state(Constants.campaign_state))
	var exploration_state := Constants.exploration_mode_state.new() as GameplayMode
	exploration_state.map = previous.map
	exploration_state.player_view_path = previous.player_view_path
	push(exploration_state)

func _resume(previous: State):
	if previous is GameplayMode:
		pop()
