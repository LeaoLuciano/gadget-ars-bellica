class_name Campaign extends State

var map: Map = null
var player_view_path := NodePath()

func _enter(_previous: State):
	# Load map
	map = Constants.map_scene.instance() as Map
	game.view.add_child(map)
	# Creating player entity
	var spawn_effect := Effect.new() \
		.with_trait(Spawn.new(Constants.player_entity_scene, Vector2(10, 10)))
	spawn_effect = RuleSolver.resolve(game.simulation, spawn_effect)
	# Add player view
	var player: ControlledBodyView = Constants.player_view_scene.instance()
	assert(player.load_from_spawn(spawn_effect.find_trait(Spawn)))
	map.add_child(player)
	player.snap_to_grid(map.get_hex_grid())
	player_view_path = player.get_path()
	map.get_camera().attach(player)
	# Begin taking turns
	push(Constants.turn_tracking_state.new())

func _leave():
	map.queue_free()

func _resume(previous: State):
	if previous.is_state(Constants.turn_tracking_state):
		switch(Constants.title_menu_state.new())
