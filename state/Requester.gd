class_name Requester extends Node

signal requested(event, request)

func request(event: String, request: Request):
	emit_signal("requested", event, request)
