class_name ExplorationMode extends GameplayMode

var player_cur_hex := Vector2.ZERO

func _enter(previous: State):
	assert(map != null)
	if previous is TurnTracking:
		# Initialize simulation
		preload_entities()
	var player: ControlledBodyView = map.get_node_or_null(player_view_path)
	assert(player != null)
	var hex_grid := map.get_hex_grid()
	var cell := hex_grid.find_closest_cell(player.global_transform.origin)
	player_cur_hex = cell.get_hex()
	print("exploration")

func _on_toggle_mode(_request: Request):
	var tactical_mode := Constants.tactical_mode_state.new() as GameplayMode
	tactical_mode.map = map
	tactical_mode.player_view_path = player_view_path
	switch(tactical_mode)

func _on_physics_tick(tick_request: TickRequest):
	var player: ControlledBodyView = map.get_node(player_view_path)
	assert(player != null)
	
	_update_bodyview_positions()
	ActionProtocol.advance_turns(game.simulation)
	
	player.control_movement(map.get_hex_grid(), tick_request.delta)
	
	var hex_grid := map.get_hex_grid()
	var cell := hex_grid.find_closest_cell(player.global_transform.origin)
	if(cell.get_hex() != player_cur_hex):
		player_cur_hex = cell.get_hex()
		ActionProtocol.commit_movement(game.simulation, player.entity_id, player_cur_hex)
