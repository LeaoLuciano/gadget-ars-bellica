class_name MainMenu extends Control

var has_saved_game := false

func _ready():
	if !has_saved_game:
		$MenuBox/ContinueButton.hide()

func _on_continue_pressed():
	$Requester.request("continue", Request.new())

func _on_new_game_pressed():
	$Requester.request("new_game", Request.new())

func _on_exit_pressed():
	$Requester.request("exit", Request.new())
