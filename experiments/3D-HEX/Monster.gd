extends Sprite3D

var hex := Vector2.ZERO

func _process_entity(map: HexGrid, delta: float):
	var target_pos := map.qr2position(hex.x as int, hex.y as int)
	translate((target_pos - translation) * 2 * delta)

func _step_turn(_map: HexGrid):
	var dir := randi() % Hex.DIR2HEX.size()
	var diff := Hex.DIR2HEX[dir] as Vector2
	var new_hex := hex + diff
	hex = new_hex
