extends Spatial

export var SPEED := 1.0
export var CAM_SPEED := .2
export var highlight_mesh: PackedScene
export var info_path: NodePath

var current_cell: HexGrid.Cell = null
var current_target_cell: HexGrid.Cell = null
var dragging_camera := false
var drag_start_angle := 0.0
var drag_start_x := 0.0

onready var target_angle := $Avatar.rotation.y as float
onready var action_mode := true

func _ready():
	for node in get_tree().get_nodes_in_group("monster"):
		var monster := node as Spatial
		var cell := $HexMap.find_closest_cell(monster.translation) as HexGrid.Cell
		monster.hex = Vector2(cell.q, cell.r)
	for node in get_tree().get_nodes_in_group("prop"):
		var prop := node as Spatial
		var cell := $HexMap.find_closest_cell(prop.translation) as HexGrid.Cell
		prop.translation = cell.translation

func _physics_process(delta):
	var horizontal := Input.get_action_strength("move_right") \
					- Input.get_action_strength("move_left")
	var vertical := Input.get_action_strength("move_down") \
				   - Input.get_action_strength("move_up")
	var dir := Vector3(horizontal, 0.0, vertical).normalized()
	if action_mode:
		$Avatar.translate(dir * SPEED * delta)
		$Avatar.playing = dir.length_squared() > .1
		var new_cell := $HexMap.find_closest_cell($Avatar.translation) as HexGrid.Cell
		if new_cell != current_cell:
			propagate_call("_step_turn", [$HexMap])
			current_cell = new_cell
	else:
		# Keep player centered on hex
		var hex := current_cell.get_hex()
		var center_pos := $HexMap.qr2position(hex.x as int, hex.y as int) as Vector3
		$Avatar.global_translate((center_pos - $Avatar.translation) * 5 * delta)
		# Calculate target hex
		var global_dir := dir.rotated(Vector3.UP, $Avatar.rotation.y)
		var target_pos := center_pos + global_dir * 2 * $HexMap.unit as float
		var target_cell := $HexMap2.find_closest_cell(target_pos) as HexGrid.Cell
		set_current_target_cell(target_cell)

func _process(delta):
	if dragging_camera:
		var mouse_pos := get_viewport().get_mouse_position()
		var diff_x := mouse_pos.x - drag_start_x
		var diff_angle := -deg2rad(diff_x * CAM_SPEED)
		target_angle = drag_start_angle + diff_angle
	var diff = target_angle - $Avatar.rotation.y
	if diff > PI:
		diff -= 2*PI
	elif diff < -PI:
		diff += 2*PI
	$Avatar.rotate_y(diff * 5 * delta)
	propagate_call("_process_entity", [$HexMap, delta])
	# Refresh info text
	var info := get_node(info_path) as Label
	if action_mode:
		info.text = "WASD - move\nMouse right button - rotate camera\nTab - swap mode"
	else:
		info.text = "WASD - choose target\nSPACE - confirm movement\nMouse right button - rotate camera\nTab - swap mode"

func _input(event):
	if event is InputEventMouseButton:
		if event.is_pressed() and event.button_index == BUTTON_RIGHT:
			dragging_camera = true
			drag_start_angle = $Avatar.rotation.y
			drag_start_x = event.position.x
		elif not event.is_pressed() and event.button_index == BUTTON_RIGHT:
			dragging_camera = false
	elif event.is_action_pressed("debug_toggle_mode"):
		action_mode = not action_mode
		set_current_target_cell(null)
	elif not action_mode and event.is_action_pressed("interact"):
		var current := current_cell.get_hex()
		var target := current_target_cell.get_hex()
		if current != target:
			current_cell = $HexMap.get_cell(target.x as int, target.y as int)
		propagate_call("_step_turn", [$HexMap])

func set_current_target_cell(cell: HexGrid.Cell):
	if cell != current_target_cell and current_target_cell != null:
		current_target_cell.set_mesh(null)
	if cell != null:
		cell.set_mesh(highlight_mesh)
	current_target_cell = cell
