extends ItemList

export(String, DIR) var mesh_dir

onready var meshes := []

# Called when the node enters the scene tree for the first time.
func _ready():
	find_all_meshes()
	clear()
	print(meshes)
	for mesh in meshes:
		add_item(mesh.name)
	select(0)

func get_selected_mesh() -> PackedScene:
	return meshes[get_selected_items()[0]].mesh

func find_all_meshes():
	meshes.clear()
	var dir := Directory.new()
	if dir.open(mesh_dir) == OK:
		#warning-ignore:return_value_discarded
		dir.list_dir_begin()
		var file_name := dir.get_next()
		while file_name != "":
			if not dir.current_is_dir() and file_name.get_extension() == "glb":
				var mesh := load(mesh_dir + "/" + file_name) as PackedScene
				if mesh != null:
					meshes.append({
						name = file_name.get_basename(),
						mesh = mesh
					})
			file_name = dir.get_next()
		dir.list_dir_end()

