tool
class_name EntitySpawn extends Position3D

const ENTITY_SPAWN_GROUP = "entity spawn"

export var hex := Vector2.ZERO

func _ready():
	add_to_group(ENTITY_SPAWN_GROUP)

func _spawn(_simulation: Simulation) -> Array:
	return []

func snap_to_grid(map: HexGrid):
	hex = map.find_closest_cell(global_transform.origin).get_hex()
	global_transform.origin = map.qr2position(hex.x as int, hex.y as int)

func create(entity_scn: PackedScene, at_hex: Vector2) -> Effect:
	return Effect.new().with_trait(Spawn.new(entity_scn, at_hex))
