class_name ControlledBodyView extends BodyView

enum {
	NO_FLAGS = 0,
	AUTO_SLIDE = 1
}

const SPEED: float = 1.0

onready var motion := Vector3.ZERO
onready var centered := false

func _process_position(hexgrid: HexGrid, _delta: float):
	if $Tween.is_active(): return
	hex = hexgrid.find_closest_cell(global_transform.origin).get_hex()

func slide_to_position(hexgrid: HexGrid):
	var target_pos := hexgrid.qr2position(hex.x as int, hex.y as int)
	$Tween.interpolate_property(self, "translation", null, target_pos, 0.25, \
		Tween.TRANS_QUAD, Tween.EASE_OUT)
	$Tween.start()
	$Sprite.set_moving_animation()
	yield($Tween, "tween_all_completed")
	centered = true

func has_focused_hex() -> bool:
	return not centered

func get_focused_hex(hexgrid: HexGrid) -> Vector2:
	var pos := global_transform.origin
	var second_closest_cell := hexgrid.find_second_closest_cell(pos)
	return second_closest_cell.get_hex()

func control_movement(hexgrid: HexGrid, delta: float, flags = NO_FLAGS):
	if $Tween.is_active(): return
	var target_pos := hexgrid.qr2position(hex.x as int, hex.y as int)
	var dist := target_pos.distance_to(transform.origin)
	if (flags & AUTO_SLIDE) and dist > hexgrid.unit * 2:
		slide_to_position(hexgrid)
	else:
		var horizontal := Input.get_action_strength("move_right") \
						- Input.get_action_strength("move_left")
		var vertical := Input.get_action_strength("move_down") \
					   - Input.get_action_strength("move_up")
		var dir := Vector3(horizontal, 0.0, vertical).normalized()
		if(centered):
			centered = !(dir.length_squared() > 0)
		_move_in_dir(dir, delta)

func _move_in_dir(norm_dir: Vector3, delta: float):
	if $Tween.is_active(): return
	motion = transform.basis.xform(norm_dir)
	#warning-ignore:return_value_discarded
	move_and_collide(motion * SPEED * delta)
	if motion.length_squared() > 0.01:
		$Sprite.set_moving_animation()
		if norm_dir.x > 0.0:
			$Sprite.flip_h = false
		elif norm_dir.x < 0.0:
			$Sprite.flip_h = true
	else:
		$Sprite.set_idle_animation()


func get_tween_active() -> bool:
	return $Tween.is_active()
