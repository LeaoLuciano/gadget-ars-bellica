tool
class_name Map extends Spatial

export(Array, Material) var highlight_materials

enum {
	HIGHLIGHT_VALID = 0,
	HIGHLIGHT_INVALID = 1
}

var highlighted_hexes: Array

func _input(event):
	if event.is_action_pressed("toggle_mode"):
		$Requester.request("toggle_mode", Request.new())
	elif event.is_action_pressed("interact"):
		$Requester.request("interact", Request.new())
	elif event.is_action_pressed("action_1"):
		$Requester.request("action", ActionRequest.new(WaitAction.new()))

func _process(delta):
	if Engine.editor_hint:
		for spawn in get_tree().get_nodes_in_group(EntitySpawn.ENTITY_SPAWN_GROUP):
			spawn.snap_to_grid($HexGrid)
	else:
		for body_view in get_tree().get_nodes_in_group(BodyView.BODY_VIEW_GROUP):
			body_view._process_position($HexGrid, delta)

func get_camera() -> MapCamera:
	return $Camera as MapCamera

func get_hex_grid() -> HexGrid:
	return $HexGrid as HexGrid

func get_hex_wireframe() -> HexGrid:
	return $HexWireframe as HexGrid

func get_movement_bounds() -> StaticBody:
	return $MovementBounds as StaticBody

func highlight_hex(hex: Vector2, type: int = HIGHLIGHT_VALID):
	var cell: HexGrid.Cell = $HexGrid.get_cell(hex.x, hex.y)
	var mesh: MeshInstance = cell.mesh.get_child(0)
	mesh.material_override = highlight_materials[type]
	highlighted_hexes.append(cell)

func unhighlight_hex():
	for cell in highlighted_hexes:
		var mesh: MeshInstance = cell.mesh.get_child(0)
		mesh.material_override = null
