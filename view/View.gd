class_name View extends Spatial

signal control_requested(request)

func emit_control_request(request: Request):
	emit_signal("control_requested", request)
