tool
class_name Effect extends Resource

var traits := []
var prevented := false

func with_trait(trait: Trait) -> Effect:
	traits.append(trait)
	return self

func find_trait(type: Script) -> Trait:
	for trait in traits:
		if trait.get_script() == type:
			return trait
	return null

func get_all_traits() -> Array:
	return traits

func is_prevented() -> bool:
	return prevented

func prevent():
	prevented = true
	return self
