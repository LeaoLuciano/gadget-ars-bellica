class_name Target extends Trait

var target_ids := []

static func get_typename() -> String:
	return "target"

func _init(the_target_ids: Array = []):
	target_ids = the_target_ids
