class_name Damage extends Trait

export var amount := 1

static func get_typename() -> String:
	return "damage"

func _init(the_amount: int):
	amount = the_amount
