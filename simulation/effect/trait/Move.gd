class_name Move extends Trait

export var moved_entity_id: int
export var to_hex: Vector2

static func get_typename() -> String:
	return "move"

func _init(entity_id: int, hex: Vector2):
	moved_entity_id = entity_id
	to_hex = hex
