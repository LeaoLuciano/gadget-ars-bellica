extends Rule

func _process_effect(_simulation: Simulation, effect: Effect) -> Effect:
	var spawn := effect.find_trait(Spawn) as Spawn
	spawn.entity = spawn.entity_scn.instance()
	return effect

func _apply_effect(simulation: Simulation, effect: Effect):
	var spawn := effect.find_trait(Spawn) as Spawn
	simulation.register_entity(spawn.entity)
	var body := spawn.entity.find_property(Body) as Body
	if body != null:
		body.hex = spawn.hex
