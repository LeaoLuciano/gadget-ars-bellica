class_name RuleSolver extends Object

static func resolve(simulation: Simulation, effect: Effect) -> Effect:
	effect = preview(simulation, effect)
	if effect.is_prevented():
		return effect
	else:
		return apply(simulation, effect)

static func preview(simulation: Simulation, effect: Effect) -> Effect:
	for trait in effect.get_all_traits():
		var group := Rule.group_for_trait_processing(trait.get_script())
		for rule in simulation.get_tree().get_nodes_in_group(group):
			if rule is Rule:
				effect = rule.process_effect(simulation, effect)
				if effect.is_prevented():
					break
		if effect.is_prevented():
			break
	Rule.clear_all_rules(simulation.get_tree())
	return effect

static func apply(simulation: Simulation, effect: Effect) -> Effect:
	for trait in effect.get_all_traits():
		var group = Rule.group_for_trait_application(trait.get_script())
		for rule in simulation.get_tree().get_nodes_in_group(group):
			if rule is Rule:
				rule.apply_effect(simulation, effect)
	Rule.clear_all_rules(simulation.get_tree())
	return effect
