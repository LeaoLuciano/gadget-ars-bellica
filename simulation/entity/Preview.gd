tool
extends Spatial

const BODY_VIEW_PREVIEW_SCENE_PATH := "res://view/map/body_view_preview/BodyViewPreview.tscn"

export var reload_preview := false setget set_reload_preview


func _ready() -> void:
	if !Engine.editor_hint:
		self.queue_free()
	else:
		set_reload_preview(true)


func set_reload_preview(_val: bool) -> void:
	for c in self.get_children():
		c.queue_free()
	
	var bvp := load(BODY_VIEW_PREVIEW_SCENE_PATH).instance() as BodyViewPreview
	bvp.entity = self.get_parent()
	self.add_child(bvp)
