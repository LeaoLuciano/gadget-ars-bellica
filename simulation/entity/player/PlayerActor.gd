class_name PlayerActor extends Actor

var next_action: Action

func set_next_action(action: Action):
	next_action = action

func get_next_action(_simulation: Simulation) -> Action:
	var action = next_action
	next_action = null
	return action
