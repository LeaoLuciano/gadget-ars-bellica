class_name NoAction extends Action

func execute(_simulation: Simulation, _entity_id: int) -> Effect:
	return null
