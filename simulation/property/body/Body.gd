#tool
class_name Body extends Property

export var sprite_scn: PackedScene
export var view: SpriteFrames = null
export var sprite_offset: Vector2

var hex := Vector2.ZERO
var view_path := NodePath()

onready var sprite: AnimatedSprite3D = null

func get_view() -> SpriteFrames:
	return view as SpriteFrames
