extends Spatial

export var spread_radius := 0.9
export var min_patches := 2
export var max_patches := 6
export var sprite_scn: PackedScene
export(Array, Texture) var textures

func _ready():
	var variation := max_patches - min_patches + 1
	var num_patches := min_patches + randi() % variation
	for _i in num_patches:
		var sprite := sprite_scn.instance() as Sprite3D
		var texture_idx := randi() % textures.size() as int
		sprite.texture = textures[texture_idx] as Texture
		sprite.translation = pick_random_position()
		add_child(sprite)

func pick_random_position() -> Vector3:
	var pos := Vector3.ZERO
	while true:
		pos.x = rand_range(-spread_radius, spread_radius)
		pos.z = rand_range(-spread_radius, spread_radius)
		if pos.length_squared() < spread_radius * spread_radius:
			break
	return pos
